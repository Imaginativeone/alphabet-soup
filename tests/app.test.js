import { describe, expect, it } from 'vitest';
// import getData from '../src/getData.js';
import getTestData from '../src/getTestData.js';
import { createWordArray, createGrid } from '../src/createDataGrid.js';

console.log('getTestData', getTestData());

describe('Application', () => {

  const someData = getTestData();
  const wordArray = createWordArray(someData);
  const grid = createGrid(wordArray);

  it ('Should get the data file', ()=>{
    expect(someData).toBeTypeOf('string');
  })

  describe('Create the array of words', () => {

    it ('Should be an array', () => {
      expect(wordArray).toContain('AAA');
      // expect(wordArray).toContain('AAB');
    });

    describe('Turn the words into letters', () => {

      it('Should be a string grid', () => {
        expect(grid[0]).toEqual(['A', 'A', 'A']);
      });
    });
  });

  describe('Get the dimensions', () => {
    it ('Should get the dimension from the wordArray', () => {
      console.log('wordArray[0]', wordArray[0]);
      expect(wordArray[0]).toContain('x');
    });
  });
});
