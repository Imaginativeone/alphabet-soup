function createWordArray(asciiData) {
  return asciiData.split('\n')
}

function createGrid(wordArray) {

  const grid = wordArray.map(word=>word.split(''));
  grid.shift();
  return grid;
}

export { createWordArray, createGrid }
