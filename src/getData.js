import data from '../data/data.js';

const getData = () => {
  return data.trim();
}

export default getData
