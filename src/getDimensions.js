const dimensions = (wordArray) => {
  const dims = {
    R: wordArray[0].split('x')[0],
    C: wordArray[0].split('x')[1]
  }
  return dims;
};

export default dimensions
