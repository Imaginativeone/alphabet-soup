let x = [-1, -1, -1,  0, 0, 0,  1, 1, 1]; // The 8 dirs from the startg pt
let y = [-1,  0,  1, -1, 0, 1, -1, 0, 1]; // (0, 0) is omitted, no movement

export {x, y}
