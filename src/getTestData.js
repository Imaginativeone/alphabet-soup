import data from '../data/testData.js';

const getTestData = () => {
  return data.trim();
}

export default getTestData
