import getData from "./src/getData.js";
import { createWordArray, createGrid } from './src/createDataGrid.js';
import getDimensions from './src/getDimensions.js';
import { x, y } from './src/directionVector.js';
import { display } from './ui/ui.js';

const asciiData = getData();
const wordArray = createWordArray(asciiData);
const grid = createGrid(wordArray);
const dimensions = getDimensions(wordArray);

function search2D(grid, row, col, word) {
  if (grid[row][col] != word[0]) return false;

  let len = word.length;

  for (let dir = 0; dir < x.length; dir++) { // Search word in 8 dirs, startg from (r, c)

    let k, rd = row + x[dir], cd = col + y[dir]; // Init startg pt for curr dir

    for (k = 1; k < len; k++) { // 1st char already checked, match remaing chars (skip idx 0)
      if (rd >= R || rd < 0 || cd >= C || cd < 0) break; // Boundary breached? break
      if (grid[rd][cd] != word[k]) break; // If not matched, break
      rd += x[dir]; cd += y[dir]; // Moving in a particular direction
    }
    if (k == len) return true; // If all chars matched, val of ? must be equal to word length
  }
  return false;
}

function patternSearches(grid, fwdWord) {

  const revWord = reverseString(fwdWord);

  const f = patternSearch(grid, fwdWord);
  const r = patternSearch(grid, revWord);

  for (let w = 0; w < f.length; w++) {
    const displayString = `${fwdWord} ${f[w]} ${r[w]}`;
    const fromElement = document.createTextNode(displayString);
    const newLineElement = document.createElement('div');
    newLineElement.appendChild(fromElement);
    display.appendChild(newLineElement);
  }
  return 'Done'
}

function patternSearch(grid, word) { // Search a given wd in a gvn matrix in all 8 dirs
  let matchesArray = [];

  for (let row = 0; row < R; row++) { // Consider every pt as a startg pt & srch gvn word
    for (let col = 0; col < C; col++) {
      if (search2D(grid, row, col, word)) {      
        matchesArray.push(`${row}:${col}`);
      }
    }
  }
  return matchesArray;
}

function reverseString(str) { return str.split("").reverse().join(""); }

let R = dimensions.R; // Rows and columns in the given grid
let C = dimensions.C;

patternSearches(grid, 'HELLO');
patternSearches(grid, 'GOOD');
patternSearches(grid, 'BYE');
